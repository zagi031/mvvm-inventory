package com.example.zagorscak.mvvminventory.viewmodels

import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zagorscak.mvvminventory.models.LogData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class LogFragmentViewModel : ViewModel() {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("logs")

    private val _logData: MutableLiveData<String> = MutableLiveData<String>(String())
    val logData: LiveData<String>
        get() = _logData
    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                _logData.value = p0.message
            }

            override fun onDataChange(p0: DataSnapshot) {
                var result = ""
                p0.children.forEach {
                    val log = LogData()
                    log.action = it.getValue(LogData::class.java)?.action.toString()
                    log.time = it.getValue(LogData::class.java)?.time.toString()
                    log.user = it.getValue(LogData::class.java)?.user.toString()
                    result += formatLog(log)
                }
                _logData.value = result
            }
        })
    }

    fun checkConnection(cm: ConnectivityManager): Boolean {
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting) {
            _toast.value = "Newest log"
        } else {
            _toast.value = "Check internet connection"
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    private fun formatLog(log: LogData): String {
        return "\u2022 [${log.user}]@${log.time} : ${log.action}\n"
    }

    fun deleteAllLogs() {
        mDbRef.removeValue()
    }
}