package com.example.zagorscak.mvvminventory.viewmodels

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class SplashScreenViewModel : ViewModel() {
    fun userLoggedIn() = FirebaseAuth.getInstance().currentUser != null
}