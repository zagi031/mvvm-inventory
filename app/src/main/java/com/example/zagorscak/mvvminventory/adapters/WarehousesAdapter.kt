package com.example.zagorscak.mvvminventory.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.example.zagorscak.mvvminventory.databinding.WarehouseBinding
import com.example.zagorscak.mvvminventory.models.Warehouse
import com.example.zagorscak.mvvminventory.listenerInterfaces.onWarehouseClickListener

class WarehousesAdapter(private val warehouseClickListener: onWarehouseClickListener) :
    RecyclerView.Adapter<WarehouseHolder>() {
    private val warehouses: MutableList<Warehouse> = mutableListOf()

    fun refreshData(warehouses: MutableList<Warehouse>) {
        this.warehouses.clear()
        this.warehouses.addAll(warehouses)
        this.notifyDataSetChanged()
    }

    fun addWarehouse(warehouse: Warehouse) {
        this.warehouses.add(warehouse)
        this.notifyItemInserted(itemCount - 1)
    }

    fun deleteWarehouse(warehouse: Warehouse) {
        val position = this.warehouses.indexOf(warehouse)
        this.warehouses.remove(warehouse)
        this.notifyItemRemoved(position)
    }

    fun getWarehouse(position: Int) = warehouses[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WarehouseHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = WarehouseBinding.inflate(layoutInflater, parent, false)
        return WarehouseHolder(binding)
    }

    override fun onBindViewHolder(holder: WarehouseHolder, position: Int) =
        holder.bind(warehouses[position], warehouseClickListener)

    override fun getItemCount() = warehouses.size
}

class WarehouseHolder(val binding: WarehouseBinding) : RecyclerView.ViewHolder(binding.root) {

    @SuppressLint("SetTextI18n")
    fun bind(warehouse: Warehouse, listener: onWarehouseClickListener) {
        binding.warehouse = warehouse
        binding.executePendingBindings()

        itemView.setOnClickListener { listener.onWarehouseClick(adapterPosition) }
        itemView.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                listener.onWarehouseLongClick(adapterPosition)
                return true
            }
        })
    }
}
