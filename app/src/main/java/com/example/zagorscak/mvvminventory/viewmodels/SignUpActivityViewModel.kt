package com.example.zagorscak.mvvminventory.viewmodels

import android.net.ConnectivityManager
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zagorscak.mvvminventory.models.SignUpData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class SignUpActivityViewModel : ViewModel() {
    private val PASSWORD_MIN_LENGTH = 8
    private val mAuth = FirebaseAuth.getInstance()

    private val _emailInputError: MutableLiveData<String> = MutableLiveData<String>()
    private val _passwordInputError: MutableLiveData<String> = MutableLiveData<String>()
    private val _successfullSignUp: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    private val _toast: MutableLiveData<String> = MutableLiveData<String>()

    val emailInputError: LiveData<String>
        get() = _emailInputError
    val passwordInputError: LiveData<String>
        get() = _passwordInputError
    val successfulSignUp: LiveData<Boolean>
        get() = _successfullSignUp
    val toast: LiveData<String>
        get() = _toast

    fun checkConnection(cm: ConnectivityManager): Boolean {
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun checkInput(newMember: SignUpData) {
        checkEmailInput(newMember.email)
        checkPasswordInput(newMember.password)
        if (_emailInputError.value!!.isEmpty() && _passwordInputError.value!!.isEmpty()) {
            signUp(newMember)
        }
    }

    fun signUp(newMember: SignUpData) {
        mAuth.createUserWithEmailAndPassword(newMember.email, newMember.password)
            .addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    val name = newMember.name
                    val surname = newMember.surname
                    val profileUpdate = UserProfileChangeRequest.Builder().setDisplayName(
                        name.trim().capitalize() + " " + surname.trim().capitalize()
                    ).build()
                    mAuth.currentUser?.updateProfile(profileUpdate)?.addOnCompleteListener { task ->
                        _successfullSignUp.value = true
                    }
                } else {
                    _toast.value = "You have been already registered"
                }
            }
    }

    private fun checkEmailInput(email: String) {
        if (email.isEmpty()) {
            this._emailInputError.value = "Field empty"
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            this._emailInputError.value = "Email not in right form"
        } else this._emailInputError.value = String()
    }

    private fun checkPasswordInput(password: String) {
        if (password.isEmpty()) {
            this._passwordInputError.value = "Field empty"
        } else if (password.length < PASSWORD_MIN_LENGTH) {
            this._passwordInputError.value = "Password too short"
        } else this._passwordInputError.value = String()
    }
}