package com.example.zagorscak.mvvminventory.views

import android.app.ProgressDialog.show
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.viewmodels.SplashScreenViewModel

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        val viewModel = ViewModelProviders.of(this).get(SplashScreenViewModel::class.java)
        Handler().postDelayed({
            if(viewModel.userLoggedIn()){
                val loggedInIntent = Intent(
                    this, MainActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(loggedInIntent)
            }
            else {
                startActivity(Intent(this, SignInActivity::class.java))
                finish()
            }
        }, 1500)
    }
}
