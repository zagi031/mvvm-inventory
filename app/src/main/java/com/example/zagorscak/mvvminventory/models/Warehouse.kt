package com.example.zagorscak.mvvminventory.models

import com.google.firebase.database.Exclude

class Warehouse{
    var id:String = ""
    var address:String = ""
    var maxCapacity:Int = 0
    var name:String = ""

    var items = HashMap<String, Item>()

    override fun toString(): String {
        return "Name: ${name}"
    }

    @Exclude
    fun getCurrentCapacity():Int{
        var result = 0
        items.values.forEach {
            result += it.quantity
        }
        return result
    }
}