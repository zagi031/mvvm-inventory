package com.example.zagorscak.mvvminventory.viewmodels

import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zagorscak.mvvminventory.loggers.FirebaseLogger
import com.example.zagorscak.mvvminventory.models.LogData
import com.example.zagorscak.mvvminventory.models.Warehouse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class WarehousesFragmentViewModel : ViewModel() {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("warehouses")

    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast
    private val _error: MutableLiveData<String> = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error
    private var _warehouses = MutableLiveData(mutableListOf<Warehouse>())
    var warehouses: LiveData<MutableList<Warehouse>>
        get() {
            return _warehouses
        }
        private set(value) {}

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                _toast.value = p0.message
            }

            override fun onDataChange(p0: DataSnapshot) {
                val warehouses = mutableListOf<Warehouse>()
                p0.children.forEach {
                    val warehouse = it.getValue(Warehouse::class.java)!!
                    warehouses.add(warehouse)
                }
                _warehouses.value = warehouses
            }
        })
    }

    fun checkConnection(cm: ConnectivityManager): Boolean {
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting) {
            _error.value = "Not connected to internet"
            _toast.value = "Check internet connection"
        } else {
            _toast.value = String()
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun getWarehouseCurrentCapacity(warehouse: Warehouse): Int {
        var currentCapacity = 0
        warehouse.items.forEach {
            currentCapacity += it.value.quantity
        }
        return currentCapacity
    }

    private fun log(warehouse: Warehouse, action: String) {
        val logData = LogData()
        logData.user = FirebaseAuth.getInstance().currentUser?.email.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy 'at' HH:mm")
        logData.time = sdf.format(Date())
        logData.action = action
        FirebaseLogger.Log(logData)
    }

    fun addWarehouse(warehouse: Warehouse) {
        mDbRef.child(warehouse.id).setValue(warehouse)
        this.log(warehouse, "created new warehouse: ${warehouse.name}")
    }

    fun deleteWarehouse(warehouse: Warehouse) {
        mDbRef.child(warehouse.id).removeValue()
        this.log(warehouse, "deleted warehouse: ${warehouse.name}")
    }

    fun editWarehouse(warehouse: Warehouse) {
        mDbRef.child(warehouse.id).setValue(warehouse)
        this.log(warehouse, "edited warehouse info on warehouse: ${warehouse.name}")
    }
}