package com.example.zagorscak.mvvminventory.loggers

import com.example.zagorscak.mvvminventory.models.LogData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class FirebaseLogger {
    companion object {
        fun Log(logData: LogData) {
            FirebaseDatabase.getInstance().reference
                .child("logs").child("${System.currentTimeMillis()}").setValue(logData)
        }
    }
}