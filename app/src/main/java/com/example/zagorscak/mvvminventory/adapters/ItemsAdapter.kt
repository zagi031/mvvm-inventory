package com.example.zagorscak.mvvminventory.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zagorscak.mvvminventory.databinding.ItemBinding
import com.example.zagorscak.mvvminventory.models.Item
import com.example.zagorscak.mvvminventory.listenerInterfaces.onItemClickListener

class ItemsAdapter(private val itemClickListener: onItemClickListener):RecyclerView.Adapter<ItemHolder>() {
    private val items:MutableList<Item> = mutableListOf()

    fun refreshData(items: MutableList<Item>){
        this.items.clear()
        this.items.addAll(items)
        this.notifyDataSetChanged()
    }

    fun addItem(item: Item){
        this.items.add(item)
        this.notifyItemInserted(itemCount-1)
    }

    fun deleteItem(item: Item){
        val position = this.items.indexOf(item)
        this.items.remove(item)
        this.notifyItemRemoved(position)
    }

    fun getItem(position: Int) = items[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemBinding.inflate(layoutInflater, parent, false)
        return ItemHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) = holder.bind(items[position], itemClickListener)
}

class ItemHolder(val binding: ItemBinding):RecyclerView.ViewHolder(binding.root){

    @SuppressLint("SetTextI18n")
    fun bind(item:Item, listener: onItemClickListener){
        binding.item = item
        binding.executePendingBindings()
        itemView.setOnClickListener{listener.onItemClick(adapterPosition)}
    }
}