package com.example.zagorscak.mvvminventory.listenerInterfaces

interface onWarehouseClickListener {
    fun onWarehouseClick(position:Int)
    fun onWarehouseLongClick(position: Int)
}