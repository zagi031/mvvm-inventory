package com.example.zagorscak.mvvminventory.views

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.viewmodels.LogFragmentViewModel
import kotlinx.android.synthetic.main.fragment_log.*

class LogFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_log, container, false)
    }

    companion object {
        fun getInstance(): LogFragment {
            return LogFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProviders.of(this).get(LogFragmentViewModel::class.java)
        val cm = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(viewModel.checkConnection(cm)){
            fbtn_deleteAllLogs.visibility = View.VISIBLE
        }
        else{
            fbtn_deleteAllLogs.visibility = View.GONE
        }

        tv_logData.movementMethod = ScrollingMovementMethod()
        viewModel.logData.observe(viewLifecycleOwner, Observer {
            tv_logData.text = it
        })

        viewModel.toast.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })

        fbtn_deleteAllLogs.setOnClickListener{
            AlertDialog.Builder(context).setTitle("Warning")
                .setMessage("Really want to delete whole log data?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    viewModel.deleteAllLogs()
                })
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                }).show()
        }
        swipeToRefresh_fragment_log.setOnRefreshListener {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit()
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}
