package com.example.zagorscak.mvvminventory.views

import android.app.Activity
import android.app.Service
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.databinding.FragmentItemBinding
import com.example.zagorscak.mvvminventory.models.Warehouse
import com.example.zagorscak.mvvminventory.viewmodels.ItemFragmentViewModel
import com.example.zagorscak.mvvminventory.viewmodels.ItemFragmentViewModelFactory
import com.example.zagorscak.mvvminventory.viewmodels.ItemsFragmentViewModel
import kotlinx.android.synthetic.main.fragment_item.*


class ItemFragment(private val warehouseID: String, private val itemID: String) : Fragment() {
    private lateinit var viewModel: ItemFragmentViewModel

    companion object {
        fun getInstance(warehouseID: String, itemID: String): ItemFragment {
            return ItemFragment(warehouseID, itemID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentItemBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_item, container, false)
        viewModel = ViewModelProviders.of(this, ItemFragmentViewModelFactory(warehouseID, itemID))
            .get(ItemFragmentViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.changeToWarehousesFragment.observe(viewLifecycleOwner, Observer {
            clearBackStack()
            fragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, WarehousesFragment.getInstance())
                ?.commit()
        })
        viewModel.toast.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })
        viewModel.warehouses.observe(viewLifecycleOwner, Observer {
            val warehouses = it
            val adapter: ArrayAdapter<Warehouse>? = context?.let {
                ArrayAdapter<Warehouse>(
                    it,
                    R.layout.support_simple_spinner_dropdown_item,
                    warehouses
                )
            }
            adapter?.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            spinner_pick_warehouse.adapter = adapter
        })
        viewModel.item.observe(viewLifecycleOwner, Observer {
            numPicker_quantity_to_move.minValue = 1
            numPicker_quantity_to_move.maxValue = it.quantity
        })
        btn_item_quantity_minus.setOnClickListener {
            if (tv_item_quantity.text.toString().toInt() > 0) {
                var value = tv_item_quantity.text.toString().toInt()
                value--
                tv_item_quantity.text = value.toString()
            }
        }
        btn_item_quantity_plus.setOnClickListener {
            if (tv_item_quantity.text.toString().toInt() < viewModel.getMaxItemQuantity()) {
                var value = tv_item_quantity.text.toString().toInt()
                value++
                tv_item_quantity.text = value.toString()
            }
        }
        btn_set_quantity.setOnClickListener {
            if (tv_item_quantity.text.toString().toInt() == 0) {
                viewModel.deleteItem()
            } else if (tv_item_quantity.text.toString().toInt() != viewModel.item.value?.quantity) {
                viewModel.setNewQuantity(tv_item_quantity.text.toString().toInt())
            }
        }
        btn_move.setOnClickListener {
            if (spinner_pick_warehouse.selectedItem != null) {
                viewModel.moveItem(
                    numPicker_quantity_to_move.value,
                    spinner_pick_warehouse.selectedItem as Warehouse
                )
            } else showToast("Invalid warehouse")
        }
        btn_rename.setOnClickListener {
            if (et_rename.text.isNotBlank()) {
                viewModel.renameItem(et_rename.text.toString().trim())
//                hide focus on edit text rename
                val imm =
                    context?.getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(et_rename.windowToken, 0)
                et_rename.text.clear()
            } else showToast("New name must not be blank")
        }
    }

    private fun clearBackStack() {
        for (i in 0..(fragmentManager?.backStackEntryCount ?: 0)) {
            fragmentManager?.popBackStack()
        }
    }

    private fun showToast(text: String) {
        if (text.isNotEmpty()) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }
    }
}
