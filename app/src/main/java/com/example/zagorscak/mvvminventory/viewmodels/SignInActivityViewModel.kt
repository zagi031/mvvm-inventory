package com.example.zagorscak.mvvminventory.viewmodels

import android.net.ConnectivityManager
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class SignInActivityViewModel : ViewModel() {
    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    private val _emailInputError: MutableLiveData<String> = MutableLiveData<String>()
    private val _passwordInputError: MutableLiveData<String> = MutableLiveData<String>()
    private val _successfullSignIn: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    private val _toast: MutableLiveData<String> = MutableLiveData<String>()

    val emailInputError: LiveData<String>
        get() = _emailInputError
    val passwordInputError: LiveData<String>
        get() = _passwordInputError
    val successfulSignIn: LiveData<Boolean>
        get() = _successfullSignIn
    val toast: LiveData<String>
        get() = _toast

    fun checkConnection(cm: ConnectivityManager): Boolean {
        val activeNetwork = cm.activeNetworkInfo
        if(activeNetwork != null && !activeNetwork.isConnectedOrConnecting){
            _toast.value = "Check internet connection"
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun checkInput(email: String, password: String) {
        checkEmailInput(email)
        checkPasswordInput(password)
        if (_emailInputError.value!!.isEmpty() && _passwordInputError.value!!.isEmpty()) {
            signIn(email, password)
        }
    }

    private fun checkEmailInput(email: String) {
        if (email.isEmpty()) {
            this._emailInputError.value = "Field empty"
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            this._emailInputError.value = "Email not in right form"
        } else this._emailInputError.value = String()
    }

    private fun checkPasswordInput(password: String) {
        if (password.isEmpty()) {
            this._passwordInputError.value = "Field empty"
        } else this._passwordInputError.value = String()
    }

    private fun signIn(email: String, password: String) {
        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    _successfullSignIn.value = true
                } else {
                    _toast.value = "Incorrect email or password"
                }
            }
    }
}