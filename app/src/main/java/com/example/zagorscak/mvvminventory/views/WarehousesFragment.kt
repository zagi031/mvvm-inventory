package com.example.zagorscak.mvvminventory.views

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.adapters.WarehousesAdapter
import com.example.zagorscak.mvvminventory.models.Warehouse
import com.example.zagorscak.mvvminventory.listenerInterfaces.onWarehouseClickListener
import com.example.zagorscak.mvvminventory.viewmodels.WarehousesFragmentViewModel
import kotlinx.android.synthetic.main.customdialog_addwarehouse.*
import kotlinx.android.synthetic.main.customdialog_addwarehouse.view.*
import kotlinx.android.synthetic.main.fragment_warehouses.*
import kotlin.concurrent.thread


class WarehousesFragment : Fragment(),
    onWarehouseClickListener {
    private lateinit var viewModel: WarehousesFragmentViewModel
    private var adapter = WarehousesAdapter(this)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_warehouses, container, false)
    }

    companion object {
        fun getInstance(): WarehousesFragment {
            return WarehousesFragment()
        }

        const val ANIMATION_DURATION: Long = 500
        const val THREAD_SLEEP_DURATION: Long = 500
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(WarehousesFragmentViewModel::class.java)
        setRecyclerView()

        viewModel.error.observe(viewLifecycleOwner, Observer {
            tv_warehouses_error.text = it
        })

        viewModel.toast.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })

        viewModel.warehouses.observe(viewLifecycleOwner, Observer {
            adapter.refreshData(it)
        })

        val cm = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (viewModel.checkConnection(cm)) {
            rv_warehouses.visibility = View.VISIBLE
            fbtn_addWarehouse.visibility = View.VISIBLE
            tv_warehouses_error.visibility = View.GONE
        } else {
            rv_warehouses.visibility = View.INVISIBLE
            fbtn_addWarehouse.visibility = View.INVISIBLE
            tv_warehouses_error.visibility = View.VISIBLE
        }

        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val wareHouse = adapter.getWarehouse(viewHolder.adapterPosition)
                AlertDialog.Builder(context)
                    .setTitle("Warning")
                    .setMessage("Really want to delete ${wareHouse.name}?")
                    .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            if (viewModel.checkConnection(cm)) {
                                onConnected()
                                adapter.deleteWarehouse(wareHouse)
                                thread(start = true) {
                                    Thread.sleep(THREAD_SLEEP_DURATION)
                                    viewModel.deleteWarehouse(wareHouse)
                                }
                            } else onNotConnected()
                        }
                    })
                    .setNegativeButton("No", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            adapter.notifyItemChanged(viewHolder.adapterPosition)
                        }
                    })
                    .setOnCancelListener {
                        adapter.notifyItemChanged(viewHolder.adapterPosition)
                    }.create().show()
            }
        }).attachToRecyclerView(rv_warehouses)

        fbtn_addWarehouse.setOnClickListener {
            val dialogView =
                LayoutInflater.from(context).inflate(R.layout.customdialog_addwarehouse, null)
            val builder = AlertDialog.Builder(context)
            builder.setView(dialogView).setTitle("Add new warehouse")
            val dialog = builder.create()
            dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
            dialog.show()
            dialog.btn_customDialog_Negative.setOnClickListener {
                dialog.dismiss()
            }
            dialog.btn_customDialog_Positive.setOnClickListener {
                val name = dialogView.et_customdialog_name.text.toString().trim()
                val address = dialogView.et_customdialog_quantity.text.toString().trim()
                val maxCapacity = dialogView.et_customdialog_maxCapacity.text.toString()
                if (name.isBlank()) {
                    dialogView.et_customdialog_name.error = "Empty field"
                }
                if (address.isBlank()) {
                    dialogView.et_customdialog_quantity.error = "Empty field"
                }
                if (maxCapacity.isBlank() || maxCapacity.toInt() < 1) {
                    dialogView.et_customdialog_maxCapacity.error =
                        "Empty field or invalid capacity"
                }
                if (name.isNotBlank() && address.isNotBlank() && maxCapacity.isNotBlank() && maxCapacity.toInt() > 0) {
                    dialog.dismiss()
                    applyInput(name, address, maxCapacity.toInt())
                }
            }
        }

        swipeToRefresh_fragment_warehouses.setOnRefreshListener {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit()
        }
    }

    private fun setRecyclerView() {
        rv_warehouses.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val animator = DefaultItemAnimator()
        animator.addDuration = ANIMATION_DURATION
        rv_warehouses.itemAnimator = animator
        rv_warehouses.adapter = this.adapter
    }

    private fun onConnected() {
        rv_warehouses.visibility = View.VISIBLE
        fbtn_addWarehouse.visibility = View.VISIBLE
        tv_warehouses_error.visibility = View.GONE
    }

    private fun onNotConnected() {
        rv_warehouses.visibility = View.INVISIBLE
        fbtn_addWarehouse.visibility = View.INVISIBLE
        tv_warehouses_error.visibility = View.VISIBLE
    }

    private fun applyInput(name: String, address: String, maxCapacity: Int) {
        if (viewModel.checkConnection(activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)) {
            onConnected()
            val newWarehouse = Warehouse()
            newWarehouse.id = System.currentTimeMillis().toString()
            newWarehouse.name = name
            newWarehouse.address = address
            newWarehouse.maxCapacity = maxCapacity
            adapter.addWarehouse(newWarehouse)
            thread(start = true) {
                Thread.sleep(THREAD_SLEEP_DURATION)
                viewModel.addWarehouse(newWarehouse)
            }
        } else onNotConnected()
    }

    override fun onWarehouseClick(position: Int) {
        fragmentManager?.beginTransaction()?.setCustomAnimations(
            R.anim.enter_from_right,
            R.anim.exit_to_left,
            R.anim.enter_from_left,
            R.anim.exit_to_right
        )?.replace(
            R.id.fragment_container,
            ItemsFragment.getInstance(adapter.getWarehouse(position).id)
        )
            ?.addToBackStack(null)?.commit()
    }

    override fun onWarehouseLongClick(position: Int) {
        val dialogView =
            LayoutInflater.from(context).inflate(R.layout.customdialog_addwarehouse, null)
        val builder = AlertDialog.Builder(context)
        builder.setView(dialogView).setTitle("Edit warehouse")
        val dialog = builder.create()
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        dialog.show()
        dialog.btn_customDialog_Negative.setOnClickListener {
            dialog.dismiss()
        }
        dialog.btn_customDialog_Positive.setOnClickListener {
            if (viewModel.checkConnection(activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)) {
                val warehouse = Warehouse()
                warehouse.id = adapter.getWarehouse(position).id
                warehouse.name = dialogView.et_customdialog_name.text.toString().trim()
                warehouse.address = dialogView.et_customdialog_quantity.text.toString().trim()
                val maxCapacity = dialogView.et_customdialog_maxCapacity.text.toString()
                var currentCapacity =
                    viewModel.getWarehouseCurrentCapacity(adapter.getWarehouse(position))
                if (warehouse.name.isBlank()) {
                    dialogView.et_customdialog_name.error = "Empty field"
                }
                if (warehouse.address.isBlank()) {
                    dialogView.et_customdialog_quantity.error = "Empty field"
                }
                if (maxCapacity.isBlank() || maxCapacity.toInt() < 1 || maxCapacity.toInt() < currentCapacity) {
                    dialogView.et_customdialog_maxCapacity.error =
                        "Empty field or invalid capacity, current capacity: ${currentCapacity}"
                }
                if (warehouse.name.isNotBlank() && warehouse.address.isNotBlank() && maxCapacity.isNotBlank() && maxCapacity.toInt() > 0 && maxCapacity.toInt() >= currentCapacity) {
                    dialog.dismiss()
                    warehouse.maxCapacity = maxCapacity.toInt()
                    warehouse.items = adapter.getWarehouse(position).items
                    viewModel.editWarehouse(warehouse)
                }
            } else onNotConnected()
        }
    }

    private fun showToast(text: String) {
        if (text.isNotEmpty()) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }
    }
}

