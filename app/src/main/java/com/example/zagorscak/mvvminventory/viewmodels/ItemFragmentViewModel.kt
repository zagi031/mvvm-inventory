package com.example.zagorscak.mvvminventory.viewmodels

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.zagorscak.mvvminventory.loggers.FirebaseLogger
import com.example.zagorscak.mvvminventory.models.Item
import com.example.zagorscak.mvvminventory.models.LogData
import com.example.zagorscak.mvvminventory.models.Warehouse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class ItemFragmentViewModelFactory(private val warehouseID: String, private val itemID: String) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ItemFragmentViewModel(warehouseID, itemID) as T
    }
}

class ItemFragmentViewModel(private val warehouseID: String, private val itemID: String) : ViewModel() {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("warehouses").child(warehouseID)
    private lateinit var warehouse: Warehouse

    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast
    private var _warehouses = MutableLiveData(mutableListOf<Warehouse>())
    var warehouses: LiveData<MutableList<Warehouse>>
        get() {
            return _warehouses
        }
        private set(value) {}
    private val _changeToWarehousesFragment: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    val changeToWarehousesFragment: LiveData<Boolean>
        get() = _changeToWarehousesFragment
    private val _item: MutableLiveData<Item> = MutableLiveData<Item>()
    var item: LiveData<Item>
        get() = _item
        set(value) {}

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                _toast.value = p0.message
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.getValue(Warehouse::class.java) == null || p0.child("items").child(itemID).getValue(Item::class.java) == null) {
                    _changeToWarehousesFragment.value = true
                } else {
                    warehouse = p0.getValue(Warehouse::class.java)!!
                    _item.value = p0.child("items").child(itemID).getValue(Item::class.java)!!
//                    if(viewItem != null){
//                        viewItem?.setUI()
//                    }
                }
            }
        })
        mDatabase.reference.child("warehouses").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                _toast.value = p0.message
            }

            override fun onDataChange(p0: DataSnapshot) {
                val warehouses = mutableListOf<Warehouse>()
                p0.children.forEach {
                    val warehouse = it.getValue(Warehouse::class.java)
                    if (warehouse != null && warehouse.id != warehouseID) {
                        warehouses.add(warehouse)
                    }
                }
                _warehouses.value = warehouses
            }
        })
    }
    fun getMaxItemQuantity(): Int {
        var warehouseOtherItemsQuantity = 0
        warehouse.items.values.forEach {
            if (it.id != item.value?.id) {
                warehouseOtherItemsQuantity += it.quantity
            }
        }
        return warehouse.maxCapacity - warehouseOtherItemsQuantity
    }
    fun setNewQuantity(newQuantity: Int) {
        if (getMaxItemQuantity() >= newQuantity) {
            mDbRef.child("items").child(itemID).child("quantity").setValue(newQuantity)
            this.log("set new quantity for ${item.value?.name} from ${item.value?.quantity} to $newQuantity in ${warehouse.name}")
        } else _toast.value = "Not enough space for selected quantity"
    }
    fun renameItem(newName: String) {
        mDbRef.child("items").child(itemID).child("name").setValue(newName)
        this.log("renamed ${item.value?.name} to $newName in ${warehouse.name}")
    }
    fun moveItem(quantity: Int, destinationWarehouse: Warehouse) {
        val destinationWarehouseFreeSpace = destinationWarehouse.maxCapacity - destinationWarehouse.getCurrentCapacity()

        if (destinationWarehouseFreeSpace >= quantity) {
            val currentItemQuantity = this.warehouse.items.get(itemID)?.quantity
            if (currentItemQuantity != null) {
                mDatabase.reference.child("warehouses").child(warehouseID)
                    .child("items")
                    .child(itemID)
                    .child("quantity").setValue(currentItemQuantity - quantity)
            }
            if (currentItemQuantity == quantity) {
                deleteItem()
            }
            var destinationWarehouseContainsThatItem = false
            var destinationItemID = System.currentTimeMillis().toString()
            var destinationItemQuantity = 0
            destinationWarehouse.items.values.forEach {
                if(it.name.equals(item.value?.name)){
                    destinationItemID = it.id
                    destinationItemQuantity = it.quantity
                    destinationWarehouseContainsThatItem = true
                }
            }

            if(destinationWarehouseContainsThatItem){
                val ref = mDatabase.reference.child("warehouses").child(destinationWarehouse.id).child("items").child(destinationItemID)
                ref.child("id").setValue("${destinationItemID}")
                ref.child("name").setValue(item.value?.name)
                ref.child("quantity").setValue(destinationItemQuantity+quantity)
            }
            else{
                val ref =
                    mDatabase.reference.child("warehouses").child(destinationWarehouse.id).child("items").child(destinationItemID)
                ref.child("id").setValue("${destinationItemID}")
                ref.child("name").setValue(item.value?.name)
                ref.child("quantity").setValue(quantity)
            }
            this.log("moved ${quantity}x ${item.value?.name} from ${this.warehouse.name} to ${destinationWarehouse.name}")
        } else _toast.value = "Not enough space in that warehouse ${destinationWarehouse.getCurrentCapacity()}/${destinationWarehouse.maxCapacity}"
    }
    fun deleteItem() {
        mDatabase.reference.child("warehouses").child(warehouse.id.toString()).child("items")
            .child(item.value?.id.toString()).removeValue()
        this.log("deleted all ${item.value?.name} from ${this.warehouse.name}")
    }

    @SuppressLint("SimpleDateFormat")
    private fun log(action: String){
        val logData = LogData()
        logData.user = FirebaseAuth.getInstance().currentUser?.email.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy 'at' HH:mm")
        logData.time = sdf.format(Date())
        logData.action = action
        FirebaseLogger.Log(logData)
    }
}