package com.example.zagorscak.mvvminventory.viewmodels

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.zagorscak.mvvminventory.loggers.FirebaseLogger
import com.example.zagorscak.mvvminventory.models.Item
import com.example.zagorscak.mvvminventory.models.LogData
import com.example.zagorscak.mvvminventory.models.Warehouse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class ItemsFragmentViewModelFactory(private val warehouseID: String) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ItemsFragmentViewModel(warehouseID) as T
    }
}

class ItemsFragmentViewModel(private val warehouseID: String) : ViewModel() {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("warehouses").child(warehouseID)
    private lateinit var warehouse: Warehouse

    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast
    private var _items = MutableLiveData(mutableListOf<Item>())
    var items: LiveData<MutableList<Item>>
        get() {
            return _items
        }
        private set(value) {}
    private val _changeToWarehousesFragment: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    val changeToWarehousesFragment: LiveData<Boolean>
        get() = _changeToWarehousesFragment

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                _toast.value = p0.message
            }

            override fun onDataChange(p0: DataSnapshot) {
                val items = mutableListOf<Item>()
                if (p0.getValue(Warehouse::class.java) == null) {
                    _changeToWarehousesFragment.value = true
                } else {
                    warehouse = p0.getValue(Warehouse::class.java)!!
                    p0.child("items").children.forEach {
                        val item = Item()
                        item.id = it.key.toString()
                        item.name = it.getValue(Item::class.java)?.name.toString()
                        item.quantity = it.getValue(Item::class.java)?.quantity!!.toInt()
                        items.add(item)
                    }
                    _items.value = items
                }
            }
        })
    }

    fun checkConnection(cm: ConnectivityManager): Boolean {
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting) {
            _toast.value = "Check internet connection"
        } else {
            _toast.value = String()
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun checkFreeSpaceOfWarehouse(item: Item): Boolean {
        val warehouseMaxCapacity = this.warehouse.maxCapacity
        if (warehouse.getCurrentCapacity() + item.quantity <= warehouseMaxCapacity) {
            return true
        } else {
            _toast.value =
                "Not enough space ${warehouse.getCurrentCapacity()}/${warehouseMaxCapacity}"
            return false
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun log(action: String) {
        val logData = LogData()
        logData.user = FirebaseAuth.getInstance().currentUser?.email.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy 'at' HH:mm")
        logData.time = sdf.format(Date())
        logData.action = action
        FirebaseLogger.Log(logData)
    }

    fun addItem(item: Item) {
        mDbRef.child("items").child(item.id).setValue(item)
        this.log("added ${item.quantity}x ${item.name} to ${warehouse.name}")
    }

    fun deleteItem(item: Item) {
        mDbRef.child("items").child(item.id).removeValue()
        this.log("deleted all ${item.name} from ${warehouse.name}")
    }

}