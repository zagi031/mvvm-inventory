package com.example.zagorscak.mvvminventory.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class MainActivityViewModel : ViewModel() {
    private val mAuth = FirebaseAuth.getInstance()

    private val _succesfulSignOut = MutableLiveData<Boolean>()
    val successfulSignOut:LiveData<Boolean>
        get () = _succesfulSignOut
    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast

    fun getCurrentUser() = mAuth.currentUser

    fun signOut() {
        mAuth.signOut()
        if (mAuth.currentUser == null) {
            _succesfulSignOut.value = true
        } else _toast.value = "Some error occured"
    }
}