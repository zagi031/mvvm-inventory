package com.example.zagorscak.mvvminventory.views

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zagorscak.mvvminventory.adapters.ItemsAdapter
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.models.Item
import com.example.zagorscak.mvvminventory.listenerInterfaces.onItemClickListener
import com.example.zagorscak.mvvminventory.viewmodels.ItemsFragmentViewModel
import com.example.zagorscak.mvvminventory.viewmodels.ItemsFragmentViewModelFactory
import kotlinx.android.synthetic.main.customdialog_additem.*
import kotlinx.android.synthetic.main.customdialog_additem.view.*
import kotlinx.android.synthetic.main.fragment_items.*
import kotlin.concurrent.thread


class ItemsFragment(private val warehouseID: String) : Fragment(),
    onItemClickListener {
    private lateinit var viewModel: ItemsFragmentViewModel
    private var adapter = ItemsAdapter(this)

    companion object {
        const val THREAD_SLEEP_DURATION: Long = 500

        fun getInstance(warehouseID: String): ItemsFragment {
            return ItemsFragment(warehouseID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_items, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this, ItemsFragmentViewModelFactory(warehouseID))
            .get(ItemsFragmentViewModel::class.java)
        setRecyclerView()

        viewModel.toast.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })

        viewModel.changeToWarehousesFragment.observe(viewLifecycleOwner, Observer {
            clearBackStack()
            fragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, WarehousesFragment.getInstance())
                ?.commit()
        })

        viewModel.items.observe(viewLifecycleOwner, Observer {
            adapter.refreshData(it)
        })

        val cm = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (viewModel.checkConnection(cm)) {
            rv_items.visibility = View.VISIBLE
            fbtn_addItem.visibility = View.VISIBLE
            tv_items_error.visibility = View.GONE
        } else {
            rv_items.visibility = View.INVISIBLE
            fbtn_addItem.visibility = View.INVISIBLE
            tv_items_error.visibility = View.VISIBLE
        }

        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val item = adapter.getItem(viewHolder.adapterPosition)
                AlertDialog.Builder(context)
                    .setTitle("Warning")
                    .setMessage("Really want to delete ${item.name}?")
                    .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            if (viewModel.checkConnection(cm)) {
                                adapter.deleteItem(item)
                                thread(start = true) {
                                    Thread.sleep(THREAD_SLEEP_DURATION)
                                    viewModel.deleteItem(item)
                                }
                            }
                        }
                    })
                    .setNegativeButton("No", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            adapter.notifyItemChanged(viewHolder.adapterPosition)
                        }
                    })
                    .setOnCancelListener {
                        adapter.notifyItemChanged(viewHolder.adapterPosition)
                    }.create().show()
            }
        }).attachToRecyclerView(rv_items)

        swipeToRefresh_fragment_items.setOnRefreshListener {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit()
        }

        fbtn_addItem.setOnClickListener {
            val dialogView =
                LayoutInflater.from(context).inflate(R.layout.customdialog_additem, null)
            val builder = AlertDialog.Builder(context)
            builder.setView(dialogView).setTitle("Add new item")
            val dialog = builder.create()
            dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
            dialog.show()
            dialog.btn_customDialog_Negative.setOnClickListener {
                dialog.dismiss()
            }
            dialog.btn_customDialog_Positive.setOnClickListener {
                val name = dialogView.et_customdialog_name.text.toString().trim()
                val quantity = dialogView.et_customdialog_quantity.text.toString()
                if (name.isBlank()) {
                    dialogView.et_customdialog_name.error = "Empty field"
                }
                if (quantity.isBlank() || quantity.toInt() <= 0) {
                    dialogView.et_customdialog_quantity.error = "Empty field or invalid quantity"
                }
                if (name.isNotBlank() && quantity.isNotBlank() && quantity.toInt() > 0) {
                    dialog.dismiss()
                    if (viewModel.checkConnection(cm)) {
                        val newItem = Item()
                        newItem.id = System.currentTimeMillis().toString()
                        newItem.name = name
                        newItem.quantity = quantity.toInt()
                        if (viewModel.checkFreeSpaceOfWarehouse(newItem)) {
                            adapter.addItem(newItem)
                            thread(start = true) {
                                Thread.sleep(THREAD_SLEEP_DURATION)
                                viewModel.addItem(newItem)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setRecyclerView() {
        rv_items.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val animator = DefaultItemAnimator()
        animator.addDuration = WarehousesFragment.ANIMATION_DURATION
        rv_items.itemAnimator = animator
        rv_items.adapter = this.adapter
    }

    private fun clearBackStack() {
        for (i in 0..(fragmentManager?.backStackEntryCount ?: 0)) {
            fragmentManager?.popBackStack()
        }
    }

    private fun showToast(text: String) {
        if (text.isNotEmpty()) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }
    }

    override fun onItemClick(position: Int) {
        fragmentManager?.beginTransaction()?.setCustomAnimations(
            R.anim.enter_from_right,
            R.anim.exit_to_left,
            R.anim.enter_from_left,
            R.anim.exit_to_right
        )?.replace(
            R.id.fragment_container,
            ItemFragment.getInstance(warehouseID, adapter.getItem(position).id)
        )?.addToBackStack(null)?.commit()
    }
}

