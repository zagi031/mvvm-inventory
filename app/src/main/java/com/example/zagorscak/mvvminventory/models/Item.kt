package com.example.zagorscak.mvvminventory.models

class Item {
    var id:String = ""
    var name:String = ""
    var quantity:Int = 0
}