package com.example.zagorscak.mvvminventory.views

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.models.SignUpData
import com.example.zagorscak.mvvminventory.viewmodels.SignUpActivityViewModel
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val viewModel = ViewModelProviders.of(this).get(SignUpActivityViewModel::class.java)

        viewModel.emailInputError.observe(this, Observer {
            til_signUp_email.error = it
        })

        viewModel.passwordInputError.observe(this, Observer {
            til_singUp_password.error = it
        })

        viewModel.toast.observe(this, Observer {
            this.showToast(it)
        })

        viewModel.successfulSignUp.observe(this, Observer {
            if (it) {
                val loggedInIntent = Intent(
                    this, MainActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(loggedInIntent)
            }
        })

        btn_signUp_signUp.setOnClickListener {
            if (viewModel.checkConnection(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)) {
                val newMember = SignUpData()
                newMember.email = et_signUp_email.text.toString()
                newMember.password = et_signUp_password.text.toString()
                newMember.name = et_name.text.toString().trim()
                newMember.surname = et_surname.text.toString().trim()
                viewModel.checkInput(newMember)
            } else showToast("Check internet connection")
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
