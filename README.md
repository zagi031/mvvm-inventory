# MVVM inventory

MVVM inventory is an Android app to manage items inside your warehouses. You can add and remove warehouses, add items, change quantities of items, remove items and move items from warehouse to another warehouse. To use this app, you need to sign up using built-in signing up form.

MVVM inventory app is part of a [final paper](https://repozitorij.etfos.hr/islandora/object/etfos:2612) and a [scientific article](https://drive.google.com/file/d/18dMf0omo3lLn08JsLBXLXjaU6NulnmKg/view), starting on page 97.

To download .apk file, click [here](https://gitlab.com/zagi031/mvvm-inventory/-/blob/master/mvvm-inventory.apk).

Technologies used: Android, Kotlin, RecyclerView, Firebase services (Authentication, Realtime database)
