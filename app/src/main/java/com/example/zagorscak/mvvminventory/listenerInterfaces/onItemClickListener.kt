package com.example.zagorscak.mvvminventory.listenerInterfaces

interface onItemClickListener {
    fun onItemClick(position:Int)
}