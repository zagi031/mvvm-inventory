package com.example.zagorscak.mvvminventory.views

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.viewmodels.SignInActivityViewModel
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        val viewModel = ViewModelProviders.of(this).get(SignInActivityViewModel::class.java)

        viewModel.emailInputError.observe(this, Observer {
            til_signIn_email.error = it
        })

        viewModel.passwordInputError.observe(this, Observer {
            til_singIn_password.error = it
        })

        viewModel.toast.observe(this, Observer {
            this.showToast(it)
        })

        viewModel.successfulSignIn.observe(this, Observer {
            if (it) {
                val loggedInIntent = Intent(
                    this, MainActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(loggedInIntent)
            }
        })

        btn_signIn_signIn.setOnClickListener {
            if (viewModel.checkConnection(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)) {
                val email = et_signIn_email.text.toString()
                val password = et_signIn_password.text.toString()
                viewModel.checkInput(email, password)
            }
        }
        tv_signIn_signUp.setOnClickListener {
            val signUpIntent = Intent(this, SignUpActivity::class.java)
            startActivity(signUpIntent)
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
