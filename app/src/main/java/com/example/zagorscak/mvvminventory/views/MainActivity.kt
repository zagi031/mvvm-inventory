package com.example.zagorscak.mvvminventory.views

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.zagorscak.mvvminventory.R
import com.example.zagorscak.mvvminventory.viewmodels.MainActivityViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header.view.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        this.viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        showToast("Welcome ${viewModel.getCurrentUser()?.displayName}")
        viewModel.successfulSignOut.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Signed out", Toast.LENGTH_SHORT).show()
                val signOutIntent = Intent(
                    this,
                    SignInActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(signOutIntent)
            }
        })
        viewModel.toast.observe(this, Observer {
            showToast(it)
        })
        setUI()
    }

    @SuppressLint("SetTextI18n")
    private fun setUI() {
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        if (!viewModel.getCurrentUser()?.displayName.isNullOrBlank()) {
            nav_view.getHeaderView(0).tv_nav_header_name.visibility = View.VISIBLE
            nav_view.getHeaderView(0).tv_nav_header_name.text =
                "User: " + viewModel.getCurrentUser()?.displayName
        }
        nav_view.getHeaderView(0).tv_nav_header_email.text =
            "Email: " + viewModel.getCurrentUser()?.email
        nav_view.setNavigationItemSelectedListener(this)
        showWarehouseFragment()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_warehouse -> showWarehouseFragment()
            R.id.nav_log -> showLogFragment()
            R.id.nav_signout -> viewModel.signOut()
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showWarehouseFragment() {
        nav_view.menu.getItem(0).isChecked = true
        clearBackStack()
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.fragment_container, WarehousesFragment.getInstance()).commit()
    }

    private fun showLogFragment() {
        nav_view.menu.getItem(1).isChecked = true
        clearBackStack()
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.fragment_container, LogFragment.getInstance()).commit()
    }

    private fun clearBackStack() {
        for (i in 0..supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
